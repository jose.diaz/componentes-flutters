import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  SliderPage({Key key}) : super(key: key);

  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {
  double _valorSlider = 400;
  bool _bloquearCheck = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Slider Page'),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 50),
        child: Column(
          children: <Widget>[
            Expanded(child: _crearImagen()),
            _crearSlier(),
            Divider(),
            _crearCkeckBox(),
            _crearSwitch(),
            Divider(),
            _crearInfo(),
          ],
        ),
      ),
    );
  }

  Widget _crearSlier() {
    return Slider(
      activeColor: Colors.indigoAccent,
      //divisions: 20,
      label: 'Tamaño de la imagen',
      value: _valorSlider,
      min: 10,
      max: 400,
      onChanged: (_bloquearCheck)
          ? null
          : (valor) {
              setState(() {
                _valorSlider = valor;
              });
            },
    );
  }

  Widget _crearInfo() {
    return ListTile(
      title: Text('Valor Slider: $_valorSlider'),
    );
  }

  Widget _crearImagen() {
    return FadeInImage(
      image: NetworkImage(
          'https://purepng.com/public/uploads/large/purepng.com-darth-vaderdarth-vaderanakin-skywalkerstar-wars-franchiseskywalker-17015277544928knzg.png'),
      placeholder: AssetImage('assets/jar-loading.gif'),
      fadeInDuration: Duration(milliseconds: 200),
      width: _valorSlider,
      fit: BoxFit.contain,
    );
  }

  Widget _crearCkeckBox() {
    // return Checkbox(
    //   activeColor: Colors.indigoAccent,
    //   value: _bloquearCheck,
    //   onChanged: (valor) {
    //     setState(() {
    //       _bloquearCheck = valor;
    //     });
    //   },
    // );

    return CheckboxListTile(
      title: Text('Bloquear Slider'),
      activeColor: Colors.indigoAccent,
      value: _bloquearCheck,
      onChanged: (valor) {
        setState(() {
          _bloquearCheck = valor;
        });
      },
    );
  }

  Widget _crearSwitch() {

    return SwitchListTile(
      title: Text('Bloquear Slider'),
      activeColor: Colors.indigoAccent,
      value: _bloquearCheck,
      onChanged: (valor) {
        setState(() {
          _bloquearCheck = valor;
        });
      },
    );  

  }
}
