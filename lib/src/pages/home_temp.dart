import 'package:flutter/material.dart';


class HomePageTemp extends StatelessWidget {
  final opcines = ['Uno','Dos','Tres','Cuatro','Cinco','Seis'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componenetes Temp'),
      ),
      body: ListView(
        children: _crearItemsCorto(),
      ),
    );
  }


  List<Widget> _crearItems()
  {
    List<Widget> Lista = new List<Widget>();
    for (String opt in opcines) {
      final tempWidget = ListTile(
        title: Text(opt),
      );
      Lista..add(tempWidget)
           ..add(Divider());
    }
    return Lista;
  }

  List<Widget> _crearItemsCorto(){
    return opcines.map( (item) {
      return Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.add_a_photo),
            title: Text(item + '!'),
            subtitle: Text('Cualquier Cosa'),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: (){
              
            },
          ),
          Divider()
        ],
      );
    }).toList();

  }
}