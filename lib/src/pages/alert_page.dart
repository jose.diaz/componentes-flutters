import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {
  const AlertPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alert Page'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Mostar Alerta'),
          onPressed: () => _mostrarAlerta(context),
          color: Colors.blue,
          textColor: Colors.white,
          shape: StadiumBorder(),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.exit_to_app),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }

  void _mostrarAlerta(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context){
        return AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          title: Text('Titulo'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
            Text('Contenido del mensaje alerta'),
            FlutterLogo(
              size: 100,
            )
          ],),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancelar'), 
              onPressed: () => Navigator.of(context).pop(),
              ),
            FlatButton(child: Text('Aceptar'), onPressed: (){},),
          ],
        );
      }
    );
  }
}
